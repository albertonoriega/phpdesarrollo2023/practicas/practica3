<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        li{
            list-style: none;
            padding: 10px;
            margin: 2px;
            width: 250px;
            font-weight: bold;
        }
    </style>
</head>
<body>
<?php

$radio =4;

$longitud = 2 * Pi() * $radio;
$area = pi() * $radio**2;
$volumen = 4/3 * pi() * $radio**3;
// redondear a 2 decimales
$longitud = round($longitud,2);
$area = round($area,2);
$volumen = round($volumen,2);

?>

<ul>
    <li>Radio: <?= $radio?></li>
    <li>Longitud de la circunferencia: <?= $longitud?> </li>
    <li>Area: <?= $area?></li>
    <li>Volumen: <?= $volumen ?></li>
</ul>    
</body>
</html>
