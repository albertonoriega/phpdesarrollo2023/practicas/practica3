<?php

$nota=11;

if ($nota>=0 && $nota<3) {
    $salida = "Muy deficiente";
} elseif ($nota < 5) {
    $salida = "suspenso";
} elseif ($nota < 6) {
    $salida = "aprobado";
} elseif ($nota < 7) {
    $salida = "bien";
} elseif ($nota < 9) {
    $salida = "notable";
} elseif ($nota < 10) {
    $salida = "Sobresaliente";
} else {
    $salida = "Nota introducida no válida";
}

echo $salida;
